https://b4builder.pkurg.ru/docs

##Table of Contents
1. Backend Page Builder
2. Frontend Content Builder
3. Static Page 
4. Marketplace themes for Bootstrap 4 Page Builder
5. Custom blocks
6. Add Google Fonts
7. Creating a couple of pages with a navigation menu
8. Page Sections
9. RTL pages  https://b4builder.pkurg.ru/docs#item-1-7

##1) Backend Page Builder

Create layout.

    <!doctype html>
    <html lang="en">
      <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        {% styles %}            
      </head>
      <body>

        {% page %}

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>    
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        {% framework extras %}
        {% scripts %}
      </body>
    </html>

Video manual
!![640x360](//www.youtube.com/embed/hvdJSPdsPaE)

##2) Frontend Content Builder

Create layout.

example **builder-layout.htm** 

    <!doctype html>
    <html lang="en">
      <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        {% styles %}            
      </head>
      <body>

        {% page %}

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>    
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        {% framework extras %}
        {% scripts %}
      </body>
    </html>
Create content file. 
**example empty main.htm**

Create cms page and add ContentBuilder component.

**example main.htm**

    title = "main"
    url = "/"
    layout = "builder-layout"
    is_hidden = 0
    robot_index = "index"
    robot_follow = "follow"

    [ContentBuilder]
    file = "main.htm"
    ==
    {% component 'ContentBuilder' %}

Go to frontend and edit your page.

Video manual
!![640x360](//www.youtube.com/embed/jeXkGdKGm_k)

##3)  Static Pages
Turn on "Use builder for Static Pages" switcher.

![](https://i.ibb.co/gRXFP7G/screenshot-b4dev-pkurg-ru-2020-02-07-17-35-34.png)



Layout example (static-layout.html):



    
    [staticPage]
    useContent = 1
    default = 0
    ==
    <!doctype html>
      <html lang="en">
      <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{ this.page.meta_title }}</title>
        <meta name="description" content="{{ this.page.meta_description }}">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        {% styles %}
      </head>
      <body>
        
        {% page %}

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>    
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        {% framework extras %}
        {% scripts %}    
      </body>
      </html>



!![640x360](//www.youtube.com/embed/iJ1LnKO6s1U)

##4)  Marketplace themes for Bootstrap 4 Page Builder
You can also use ready-made themes specially made for Bootstrap 4 Page Builder
- [Industry](https://octobercms.com/theme/pkurg-pkurg-industry)
- [Lambda](https://octobercms.com/theme/pkurg-lambda)
- [Oneder](https://octobercms.com/theme/pkurg-oneder)
- [Nexus](https://octobercms.com/theme/pkurg-nexus)
- [Eduland](https://octobercms.com/theme/pkurg-eduland)

!![640x360](//www.youtube.com/embed/BJwL-bQQxGw)

##5)  Custom blocks
Go to Builder settings and add your custom blocks code

![](https://i.ibb.co/w0pFZKT/screenshot-demo-pkurg-ru-2019-10-27-05-48-30.png)

![](https://i.ibb.co/sW0VYTY/screenshot-demo-pkurg-ru-2019-10-27-05-49-35.png)

----

##6)  Add Google Fonts

Video manual 
!![640x360](//www.youtube.com/embed/9gG5qivNoQM)

Add 'Poppins', sans-serif  example



    var head = Editor.Canvas.getDocument().head;
    head.insertAdjacentHTML('beforeend', `<style>
    @import url('https://fonts.googleapis.com/css?family=Poppins&display=swap');
    </style>`);

    Editor.on('load', function () {
        styleManager = Editor.StyleManager;
        
        fontProperty = styleManager.getProperty('typography', 'font-family');
        
        var list = [];
        fontProperty.set('list', list);
        list = [                     
            fontProperty.addOption({value: "Arial, Helvetica, sans-serif", name: 'Arial'}),
            fontProperty.addOption({value: "Arial Black, Gadget, sans-serif", name: 'Arial Black'}),
            fontProperty.addOption({value: "Brush Script MT, sans-serif", name: 'Brush Script MT'}),
            fontProperty.addOption({value: "Comic Sans MS, cursive, sans-serif", name: 'Comic Sans MS'}),
            fontProperty.addOption({value: "Courier New, Courier, monospace", name: 'Courier New'}),
            fontProperty.addOption({value: "Georgia, serif", name: 'Georgia'}),
            fontProperty.addOption({value: "Helvetica, serif', cursive", name: 'Helvetica'}),
            fontProperty.addOption({value: "Impact, Charcoal, sans-serif", name: 'Impact'}),
            fontProperty.addOption({value: "Lucida Sans Unicode, Lucida Grande, sans-serif", name: 'Lucida Sans Unicode'}),
            fontProperty.addOption({value: "Tahoma, Geneva, sans-serif", name: 'Tahoma'}),
            fontProperty.addOption({value: "Times New Roman, Times, serif", name: 'Times New Roman'}),
            fontProperty.addOption({value: "Trebuchet MS, Helvetica, sans-serif", name: 'Trebuchet MS'}),
            fontProperty.addOption({value: "Verdana, Geneva, sans-serif", name: 'Verdana'}),
            fontProperty.addOption({value: "'Poppins', sans-serif", name: 'Poppins'}),
            
        ];
        fontProperty.set('list', list);
        styleManager.render();
    });

##7) Creating a couple of pages with a navigation menu

!![640x360](//www.youtube.com/embed/9fK7NzCL2Yk)

##8) Page Sections

!![640x360](//www.youtube.com/embed/PeyZh6a5JOM)