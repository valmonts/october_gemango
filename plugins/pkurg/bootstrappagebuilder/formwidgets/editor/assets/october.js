function PB4October(editor, options){

//console.log(options);

const pfx = editor.getConfig('stylePrefix');
  const modal = editor.Modal;
  const codeViewer = editor.CodeManager.getViewer('CodeMirror').clone();
  const container = document.createElement('div');
  const importLabel = 'Import';
  const importCnt = ' ';
  let viewerEditor = codeViewer.editor;



//Import 
editor.Panels.addButton('options',{
  id: 'import-editor',
       className: 'fa fa-download', //I will change the icon to a better icon later if this code works
       //label: 'Save to server',
       command: {
        
          run(editor, sender) {
            sender && sender.set('active', false);

  // Init import button
  const btnImp = document.createElement('button');
  btnImp.type = 'button';
  btnImp.innerHTML = 'Import';
  btnImp.className = `${pfx}btn-prim ${pfx}btn-import`;
  btnImp.onclick = e => {
    editor.setComponents(viewerEditor.getValue().trim());
    modal.close();
  };

  // Init code viewer
  codeViewer.set({ ...{
    codeName: 'htmlmixed',
    theme: 'default',
    class: 'ty',
    readOnly: 0
  }, ...{}});


      if (!viewerEditor) {
        const txtarea = document.createElement('textarea');

        if (importLabel) {
          const labelEl = document.createElement('div');
          labelEl.className = `${pfx}import-label`;
          labelEl.innerHTML = 'Paste here your HTML/CSS and click Import';
          container.appendChild(labelEl);
        }

        container.appendChild(txtarea);
        container.appendChild(btnImp);
        codeViewer.init(txtarea);
        viewerEditor = codeViewer.editor;
      }

      modal.setTitle('Import');
      modal.setContent(container);
      const cnt = typeof importCnt == 'function' ? importCnt(editor) : importCnt;
      codeViewer.setContent(cnt || '');
      modal.open().getModel()
      .once('change:open', () => editor.stopCommand(this.id));
      viewerEditor.refresh();
    },

    stop() {
      
      modal.close();
    }
      

     },
     attributes: { title: 'Import'},
   });




//October Partial Block

    editor.DomComponents.addType('october-partial', {
        isComponent: el => {
            if (el.attributes) {

                if(el.attributes.getNamedItem('october-partial')) {
                    var r = el.attributes.getNamedItem('october-partial').value;

                    if(r == '1'){

                        return true;
                    }

                }

            }

        },
        model: {
            defaults: {
                traits: [
                'id',
                {
                    type: 'select',
                    name: 'partial',

                    options: options.partials
                },

                ]

            },
            init() {
                this.on('change:attributes:partial', this.handleChange);
                this.handleChange();
            },
            handleChange() {
                const partial = this.getAttributes().partial;



                if(this.getAttributes()['october-partial'] == '1'){           

                    if (partial) {


                        this.attributes['content'] = '';
                        this.components(` {% partial '${partial}' %}`);



                    } else {
                        this.attributes['content'] = '';
                        this.components(`Select partial`);
                    }
                }
            }
        }
    });

    editor.BlockManager.add('october-partial-block', {
        label: 'October CMS partial',
        category: 'October CMS',        
        content:`<div data-gjs-type="october-partial" october-partial="1" ></div>`,
        attributes: {
            class: 'fa fa-code'
        }
    });

//October Content Block

    editor.DomComponents.addType('october-content', {
        isComponent: el => {
            if (el.attributes) {

                if(el.attributes.getNamedItem('october-content')) {
                    var r = el.attributes.getNamedItem('october-content').value;

                    if(r == '1'){

                        return true;
                    }

                }

            }

        },
        model: {
            defaults: {
                traits: [
                'id',
                {
                    type: 'select',
                    name: 'content',

                    options: options.contents
                },

                ]

            },
            init() {
                this.on('change:attributes:content', this.handleChange);
                this.handleChange();
            },
            handleChange() {
                const content = this.getAttributes().content;



                if(this.getAttributes()['october-content'] == '1'){           

                    if (content) {


                        this.attributes['content'] = '';
                        this.components(` {% content '${content}' %}`);



                    } else {
                        this.attributes['content'] = '';
                        this.components(`Select content`);
                    }
                }
            }
        }
    });


    editor.BlockManager.add('october-content-block', {
        label: 'October CMS Content',
        category: 'October CMS',        
        content:`<div data-gjs-type="october-content" october-content="1" ></div>`,
        attributes: {
            class: 'fa fa-code'
        }
    });










editor.DomComponents.addType('Static-Pages-Snippet', {
  // Make the editor understand when to bind `my-input-type`
  isComponent: el => el.tagName === 'FIGURE',

  // Model definition
  model: {
    // Default properties
    defaults: {
      tagName: 'figure',      
      traits: [
      {
        label: 'Snippet CODE',
        type: 'select',
        
        name: 'data-snippet',
         options: options.snippets,
        
      },
      

      ],
    }
  }
});







//Generate Static Pages Snippets Blocks

    if (options.snippets) {

        options.snippets.forEach((element) => {


            var snipcode = element['code'];
            var snipname = element['name'];
            console.log(snipcode);


            editor.BlockManager.add('static-pages-snippet-' + snipcode, {
                label: snipname,
                category: 'Static Pages Snippets',
                content: `
                <style>
                figure {
                    height: 40px;
                    border-style: dashed;
                    border-width: 1px;
                    border-color: #757575;  
                    margin: 0;
                }
                figure[data-snippet="`+snipcode+`"]::after{
                    content:"🗏 `+ snipname +`";
                    position:relative;
                    top: 5px;
                    margin-left:5px;
                  }
                </style>
                <figure data-snippet="` + snipcode + `"></figure>`,
                attributes: {
                    class: 'fa fa-code'
                }
            });


                var tr = [];
                element['properties'].forEach((trait) => {

                let type = 'text';
                let dop = [];

                if (trait['type'] == 'string') {
                    type = 'text';
                }
                if (trait['type'] == 'checkbox') {
                    type = 'checkbox';
                }
                if (trait['type'] == 'dropdown') {

                    type = 'select';

                    for (key in trait['options']) {

                        dop.push(trait['options'][key]);
                    }

                }
            
                tr.push({
                        type: type,
                        name: 'data-property-' + trait['property'],
                        label: trait['title'],
                        options: dop,
                        placeholder: trait['default'],
                        valueTrue: 1,
                        valueFalse: 0
                    });
    
            });

                //console.log(tr);

              editor.DomComponents.addType('Static-Pages-Snippet-' + snipcode, {

                    isComponent(el) {
                        let result;
                        var tag = el.tagName;

                        if (tag == 'FIGURE') {

                            var cl = el.getAttribute("data-snippet");

                            if (cl == snipcode) {

                                return true;
                            }
                        }

                    },
                    model: {
                        defaults: {
                            traits: tr,
                        }
                    }
                });

        });

}




/*
editor.DomComponents.addType('Static-Pages-Snippet', {
  // Make the editor understand when to bind `my-input-type`
  isComponent: el => el.tagName === 'FIGURE',

  // Model definition
  model: {
    // Default properties
    defaults: {
      tagName: 'figure',      
      traits: [
      {
        label: 'Snippet CODE',
        type: 'select',
        
        name: 'data-snippet',
         options: options.snippets,
        
      },
      

      ],
    }
  }
});


editor.BlockManager.add('static-snippet-block', {
    label: 'Snippet',
    category: 'Static Pages',
    content:`
    <style>
    figure {
        height: 40px;
        border-style: dashed;
        border-width: 1px;
        border-color: #757575;  
        margin: 0;
    }
    </style>
    <figure data-snippet=""></figure>`,
    attributes: {
        class: 'fa fa-code'
    }
});

*/




}