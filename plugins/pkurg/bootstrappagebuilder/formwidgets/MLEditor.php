<?php namespace Pkurg\BootstrapPageBuilder\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Cms\Classes\Content;
use Cms\Classes\Partial;
use Config;
use Pkurg\BootstrapPageBuilder\Models\Settings;
use RainLab\Translate\Models\Locale;
use Storage;

class MLEditor extends FormWidgetBase
{

    use \RainLab\Translate\Traits\MLControl;

    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'BootstrapPageBuilder';

    // public function getScripts()
    // {
    //     return 'scripts';
    // }

    public function init()
    {
        $this->initLocale();
    }

    public function getSaveValue($value)
    {
        return $this->getLocaleSaveValue($value);
    }

    public function render()
    {

        $this->vars['locales'] = Locale::listAvailable();

        $lang = \Lang::getLocale();
        $LANG = strtoupper($lang);

        $this->vars['l'] = Locale::getDefault()->code;

        $this->vars['lang'] = $lang . '-' . $LANG;

        $this->prepareLocaleVars();

        $curdir = url('/');

        if (Settings::get('debug_mode')) {
            $version = uniqid();
        } else {
            $version = \System\Models\PluginVersion::getVersion('Pkurg.BootstrapPageBuilder');
        }

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/beautify-html.js?' . $version);

        $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/mleditor/assets/css/grapes.min.css?' . $version);

        $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/mleditor/assets/css/grapick.min.css?' . $version);

        $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/mleditor/assets/builder.css?' . $version);

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/mleditor/assets/grapes.min.js?' . $version);

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/mleditor/assets/vkbeautify.js?' . $version);

        $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-plugin.js?' . $version);

        $this->addJs('https://b4builder.pkurg.ru/data/blog.js?' . $version);

        $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-templates-plugin.js?' . $version);

        $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-plugin-snippets.js?' . $version);

        $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-plugin-landing-pages.js?' . $version);

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/mleditor/assets/grapes-custom-code.js?' . $version);

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/mleditor/assets/grapesjs-tui-image-editor.min.js?' . $version);

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/mleditor/assets/grapesjs-style-gradient.min.js?' . $version);

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/mleditor/assets/grapesjs-style-filter.min.js?' . $version);

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapesjs-plugin-export.min.js?' . $version);

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapesjs-blocks-bootstrap4.min.js?' . $version);

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/october.js?' . $version);

        $u = uniqid();

        $this->addJs(url('b4bulder-custom-blocks.js?v=' . $u));

        $defaultdisk = Config::get('filesystems.default');

        $path = url(Config::get('filesystems.disks.' . $defaultdisk . '.url'));

        $files = Storage::allFiles('media/BuilderUploader');

        $purl = parse_url($path);

        $this->vars['path'] = $purl['path'];

        $this->vars['files'] = $files;

        $this->vars['formid'] = $this->formField->idPrefix;

        $this->vars['name'] = $this->getFieldName();
        $this->vars['value'] = $this->getLoadValue();

        $this->vars['curdir'] = url('/');

        $this->vars['pagename'] = $this->model->fileName;

        //$this->vars['customblocks'] = Settings::get('customblocks');

        $this->vars['mediapath'] = config('cms.storage.media.path');

        $this->vars['partials'] = $this->getPartials();

        $this->vars['contents'] = $this->getContents();

        $this->vars['snippets'] = $this->getSnippets();

        $this->vars['octobermedia'] = Settings::get('useoctobermedia');

        $defaultdisk = Config::get('filesystems.default');

        $path = url(Config::get('filesystems.disks.' . $defaultdisk . '.url'));

        $purl = parse_url($path);

        $mediadir = Config::get('cms.storage.media.folder');

        //$Url = $purl['path'] . '/' . $mediadir . '/BuilderUploader/' . $q . $fileName;
        $this->vars['mediadir'] = $purl['path'] . '/' . $mediadir;

        $this->vars['buildercanvasassets'] = Settings::get('buildercanvasassets');

        if ($this->vars['buildercanvasassets']) {

            if (Settings::get('builder_styles')) {
                $this->vars['builder_styles'] = \Twig::parse(Settings::get('builder_styles'));
            } else {
                $this->vars['builder_styles'] = '';
            }

            if (Settings::get('builder_scripts')) {
                $this->vars['builder_scripts'] = \Twig::parse(Settings::get('builder_scripts'));
            } else {
                $this->vars['builder_scripts'] = '';
            }
        }

        return $this->makePartial('builder');
    }

    public function getPartials()
    {

        $a = Partial::sortBy('baseFileName')->lists('baseFileName', 'fileName');

        $b = [];

        foreach ($a as $key => &$value) {

            $l = strlen($value);
            if (mb_substr($value, $l - 3, 1) == '.') {

                $s = mb_substr($value, 0, $l - 3);

            } else {

                $b[$key] = $value;

            }

        }

        return $b;

    }

    public function getContents()
    {

        $a = Content::sortBy('baseFileName')->lists('baseFileName', 'fileName');

        $b = [];

        foreach ($a as $key => &$value) {

            $l = strlen($value);
            if (mb_substr($value, $l - 3, 1) == '.') {

                $s = mb_substr($value, 0, $l - 3);
                //$b[$s] = $s;

            } else {

                $b[$key] = $value;

            }

        }

        return $b;
    }

    public function getSnippets()
    {

        $r = [];

        if (\System\Classes\PluginManager::instance()->exists('RainLab.Pages')) {

            $theme = \Cms\Classes\Theme::getActiveTheme();
            $partialSnippetMap = \RainLab\Pages\Classes\SnippetManager::instance()->getPartialSnippetMap($theme);

            $listSnippets = \RainLab\Pages\Classes\SnippetManager::instance()->listSnippets($theme);

            foreach ($listSnippets as $key => $value) {

                $r[] = ['code' => $value->code, 'name' => $value->getName(), 'properties' => $value->getProperties()];
            }
        }

        return $r;

    }

}
